#!/usr/bin/env python
# pylint: disable=W0613, C0116
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging, sys, requests, time, math

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

bot_token = sys.argv[1]

# Ajout du log pour permettre de gérer les exceptions
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

STATE_TRANSPORT = "wait_for_transport"
STATE_START = "wait_for_choice"
STATE_RESTAURANTS_CHOICE = "wait_for_restaurants"
STATE_GOOUT_CHOICE = "wait_for_divertising"
STATE_RESTAURANTS_RESULT = "wait_for_restaurant"
STATE_RESTAURANT_DETAILS = "wait_for_restaurant_details"
STATE_GOOUT_RECOMMENDATIONS = "wait_for_recommendations"
STATE_MUSEUMS = "wait_for_museum"
STATE_BARS = "wait_for_bars"
STATE_CLUBS = "wait_for_clubs"


def start(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Eat', 'Go out'],
    ]
    displayMessage(update, reply_keyboard, 'Hi! My name is GregCrea Bot. I will hold a conversation with you. '
        'Send /cancel to stop talking to me.\n\n'
        'What do you want to do, eat or go out ?')

    return STATE_START


def choice_restaurant(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Italian', 'Chinese', ' American'],
        ['Japanese', 'Colombian'],
        ['Back'],
    ]
    displayMessage(update, reply_keyboard, 'Fine, what kind of food do you want ?')


    return STATE_RESTAURANTS_CHOICE



def choice_fun(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Museums', 'Bars', 'Clubs'],
        ['Back'],
    ]

    displayMessage(update, reply_keyboard, 'Good, where do you want to go ? ')

    return STATE_GOOUT_CHOICE

def italian_restaurants(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Gioconda', '3 Fratelli', 'Da Toni'],
        ['Back'],
    ]

    displayMessage(update, reply_keyboard, 'Choose one restaurant ')

    return STATE_RESTAURANTS_RESULT


def displayRestaurant(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Back'],
    ]

    displayMessage(update, reply_keyboard, 'Dans ce restaurant familial, la convivialité est le maître mot! Nicola, Cristina et leurs collaborateurs mettent un point d\'honneur à soigner l\'accueil et surtout leurs plats. Une des spécialités de la maison sont les pâtes fraîches, faites à la minute! Le tartare servi dans votre assiette est coupé au couteau et la carte vous offre un choix de plus de 30 sortes de pizzas! Nous proposons quelque 130 qualités de vins que nous vous invitons à découvrir.\n' 
            'Nous nous réjouissons de vous accueillir sur notre belle terrasse fleurie et ombragée en été, et dans notre grande véranda lumineuse les jours moins cléments.\n\n'
            'Le restaurant la Gioconda reste ouvert 7 jours sur 7.\n'
            'Phone : 022.798.96.05\n'
            'Schedules: 11:30 a.m. to 2:30 p.m. and from 5:30 p.m. to 11:00 p.m.\n'
            '81, Avenue Louis-Casaï - 1216 Cointrin\n\n'
            'https://www.restaurant-pizzeria-la-gioconda.ch/presentation-restaurant-gioconda'
    )

    update.message.reply_location('46.22589389435825', '6.105770284748251')

    return STATE_RESTAURANTS_DETAILS


def museums(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Back']
    ]

    displayMessage(update, reply_keyboard, 'Musée d\'art et d\'histoire de Genève\n'
                'Phone : +41 22 418 26 00\n'
                'Schedules : Open from 11am to 6pm, closed on Mondays. \n'
                '2, Rue Charles-Galland - 1206 Genève\n\n'
                'http://institutions.ville-geneve.ch/fr/mah/'
    )

    displayMessage(update, reply_keyboard, 'Musée d\'histoire des sciences de Genève\n'
                'Phone : +41 22 418 50 60\n'
                'Schedules : Open from 10am to 5pm \n'
                '128, Rue de Lausanne - 1202 Genève\n\n'
                'http://institutions.ville-geneve.ch/fr/mhn/'
    )

    displayMessage(update, reply_keyboard, 'Musée d\'histoire naturelles de Genève\n'
                'Phone : +41 22 418 64 00\n'
                'Schedules : Open from 10 a.m. to 5 p.m., except Mondays. \n'
                '1, Route de Malagnou - 1208 Genève\n\n'
                'http://institutions.ville-geneve.ch/fr/mhn/'
    )

    return STATE_GOOUT_RECOMMENDATIONS




def bars(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Back']
    ]

    displayMessage(update, reply_keyboard, 'Casa Tiki\n'
                'Phone : +41 76 703 21 94\n'
                'Schedules :\nTuesday to Thursday open from 5pm to 1am\nFrom Friday to Saturday open from 5pm to 2am\n'
                '11, Rue du Vélodrome - 1205 Genève\n\n'
                'https://casatiki.ch/'
    )

    displayMessage(update, reply_keyboard, 'Kulture Lounge Bar\n'
                'Phone : +41 76 703 21 94\n'
                'Schedules :\nTuesday to Thursday open from 5pm to 1am\nFrom Friday to Saturday open from 5pm to 2am\n'
                '47, Route des Acacias - 1227 Genève\n\n'
                'https://www.kulture.ch/'
    )

    displayMessage(update, reply_keyboard, 'Boléro Bar\n'
                'Phone : +41 22 731 69 40\n'
                'Schedules :\nTuesday to Thursday open from 7am to 2am\nFrom Friday to Saturday open from 10am to 4am\n'
                '30, Rue de Berne - 1201 Genève\n\n'
                'https://bolerobar.business.site/'
    )



    return STATE_GOOUT_RECOMMENDATIONS




def clubs(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Back']
    ]

    displayMessage(update, reply_keyboard, 'L\'events\n'
                'Schedules :Thursday open from 9:30 pm to 2 am From Friday to Saturday open from 10 pm to 4 am\n'
                '7, Rue Caroline - 1227 Genève\n\n'
                'http://www.leventsbar.ch/'
    )

    displayMessage(update, reply_keyboard, 'Bypass\n'
                'Phone : +41 22 300 65 65\n'
                'Schedules: Wednesday to Saturday open from 11pm to 5am.\n'
                '1, Carrefour de l\'étoile - 1227 Genève\n\n'
                'https://bypassclub.ch/'
    )

    displayMessage(update, reply_keyboard, 'Java Club\n'
                'Phone :\n+41 79 661 43 34\n+41 79 456 79 70\n'
                'Schedules:  Wednesday to Saturday open from 11pm to 5am.\n'
                '30, Rue de Berne - 1201 Genève\n\n'
                'http://www.javaclub.ch/'
    )

    return STATE_GOOUT_RECOMMENDATIONS


def transport(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Back']
    ]
    displayMessage(update, reply_keyboard, 'Hi, send me your location')

    return STATE_TRANSPORT


def afficher_arrets(arrets, update):
    for arret in arrets['stations']:
        if arret['id'] is not None:
            update.message.reply_text("/stop" + arret['id'] + " - " + arret['name'])

def calculer_temps(timestamp):
    seconds = timestamp - time.time()
    minutes = seconds/60;
    minutes = math.floor(minutes)

    if minutes < 0:
        return "Trop tard"

    if(minutes < 3):
        return "Presque parti"

    return "{} min".format(minutes)


# Récupération des actions à faire

def rechercher_par_localisation(update: Update, context: CallbackContext) -> None:
    location = update.message.location
    arrets = appeler_opendata("locations?x={}&y={}".format(location.longitude, location.latitude) )
    afficher_arrets(arrets, update)

def rechercher_par_nom(update: Update, context: CallbackContext) -> None:
    arrets = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(arrets, update)


def afficher_horaires(update: Update, context: CallbackContext) -> None:
    arret_id = update.message.text[5:]
    prochains_departs = appeler_opendata("stationboard?id={}&limit=10".format(arret_id))

    reponse = "Voici les prochains départs:\n\n"

    for depart in prochains_departs["stationboard"]:
        stop = depart['stop']
        reponse = reponse + calculer_temps(stop['departureTimestamp']) + ' ' + depart['number'] + " -> " + depart['to'] + "\n"

    reponse = reponse + "\nPour actualiser: /stop{}".format(arret_id)
    update.message.reply_text(reponse)


def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Bye! I hope we can talk again some day.', reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END

def displayMessage(update: Update, reply_keyboard, str) -> None:
    update.message.reply_text(
        str,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True ),
    )

def appeler_opendata(path):
    url = "https://transport.opendata.ch/v1/" + path
    result = requests.get(url)
    print(url)
    return result.json()

# Create the Updater and pass it your bot's token.
updater = Updater(bot_token)

# Get the dispatcher to register handlers
dispatcher = updater.dispatcher

# Add conversation handler
conv_handler_geneva = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        STATE_START: [
            MessageHandler(Filters.regex('^(Eat)$'), choice_restaurant),
            MessageHandler(Filters.regex('^(Go out)$'), choice_fun)
        ],
        STATE_RESTAURANTS_CHOICE: [
            MessageHandler(Filters.regex('^(Italian)$'), italian_restaurants),
            MessageHandler(Filters.regex('^(Chinese)$'), italian_restaurants),
            MessageHandler(Filters.regex('^(American)$'), italian_restaurants),
            MessageHandler(Filters.regex('^(Japanese)$'), italian_restaurants),
            MessageHandler(Filters.regex('^(Colombian)$'), italian_restaurants),
            MessageHandler(Filters.regex('^(Back)$'), start),
        ],
        STATE_RESTAURANTS_RESULT: [
            MessageHandler(Filters.regex('^(Gioconda)$'), displayRestaurant),
            MessageHandler(Filters.regex('^(3 Fratelli)$'), displayRestaurant),
            MessageHandler(Filters.regex('^(Da Toni)$'), displayRestaurant),
            MessageHandler(Filters.regex('^(Back)$'), choice_restaurant),
        ],
        STATE_RESTAURANT_DETAILS: [
            MessageHandler(Filters.regex('^(Back)$'), italian_restaurants),
        ],
        STATE_GOOUT_CHOICE: [
            MessageHandler(Filters.regex('^(Museums)$'), museums),
            MessageHandler(Filters.regex('^(Bars)$'), bars),
            MessageHandler(Filters.regex('^(Clubs)$'), clubs),
            MessageHandler(Filters.regex('^(Back)$'), start),
        ],
        STATE_GOOUT_RECOMMENDATIONS: [
            MessageHandler(Filters.regex('^(Back)$'), choice_fun),
        ],
    },
    fallbacks=[
        CommandHandler('cancel', cancel)
    ],
)

# Add conversation handler
conv_handler_transport = ConversationHandler(
    entry_points=[CommandHandler('transport', transport)],
    states={
        STATE_TRANSPORT: [
            MessageHandler(Filters.location, rechercher_par_localisation),
            MessageHandler(Filters.command, afficher_horaires),
            MessageHandler(Filters.text, rechercher_par_nom)
        ]
    },
    fallbacks=[
        CommandHandler('cancel', cancel)
    ],
)


dispatcher.add_handler(conv_handler_geneva)
dispatcher.add_handler(conv_handler_transport)


# Start the Bot
updater.start_polling()

# Run the bot until you press Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT. This should be used most of the time, since
# start_polling() is non-blocking and will stop the bot gracefully.
updater.idle()

